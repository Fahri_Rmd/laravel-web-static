<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/Home', 'HomeController@home');

Route::get('/Register', 'AuthController@Register');

Route::get('/Welcome', 'AuthController@Welcome');

Route::get('/master', function() {
    return view('adminLTE.master');
});

Route::get('/', function(){
    return view('Table.tables');
});

Route::get('/data-tables', function(){
    return view('Table.data-tables');
});
