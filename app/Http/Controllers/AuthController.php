<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function Register(){
        return view('Register');
    }

    public function Welcome(Request $request){
        //dd($request->all());
        $Depan = $request->first_name;
        $Belakang = $request->last_name;

        return view("SelamatDatang", compact('Depan','Belakang'));
    }
    
}